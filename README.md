**Demandware SDAPI Debugger Interface**

You may clone this repo *npm update*

cd dw-debugger/

node index.js  -- Runs on port 3000

to get all the dependencies needed.

The DW Debugger will use Node.js to communicate over the DW SDAPI creating a generic interface for other IDE/Dev Tools to communicate.

![CaptureDW-SDAPI.PNG](https://bitbucket.org/repo/RknXyy/images/570303628-CaptureDW-SDAPI.PNG)